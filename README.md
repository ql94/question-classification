### 本地测试

本地采用nginx + uWSGI + Flask + Theano的Docker container环境。

docker-host虚拟机需要有以下配置(`${project_home}`换成你的本地项目路径)：
```ruby
d.vm.synced_folder "${project_home}", "/question-similarity"
d.vm.network "forwarded_port", guest: 5001, host: 5001
```

```bash
# 登上docker-host机器
vagrant ssh #{docker_host_id}
# 到Dockerfile所在目录，让docker构建image
# 如果已经有该image就可以跳过
cd /question-similarity && docker build --rm --tag theano-app .  
# 启动container
docker run -it -p 5001:80 -v /question-similarity:/opt/question-similarity --name question-similarity theano-app bash
# 运行nginx
rm /etc/nginx/sites-enabled/default && ln -s /opt/question-similarity/nginx-app.conf /etc/nginx/sites-enabled/ && nginx
# 运行uwsgi
uwsgi --ini uwsgi.ini
```

这样在本地就可以通过http://localhost:5001/question-similarity去访问了。

### 阿里云部署

阿里云上采用nginx + uWSGI emperor + Flask + Theano环境。

项目位于`/opt/question-similarity`。当项目更新之后，需要重启应用时，`touch /etc/uwsgi/vassals/question-similarity.ini`即可，uwsgi emperor会重新加载应用。`/etc/uwsgi/vassals/question-similarity.ini`是个symbolic link，指向`/opt/question-similarity/app/uwsgi.ini`。

`/etc/uwsgi/vassals/question-similarity.ini`和以下配置文件如果在阿里云上已经存在了就不需要再动了。

##### 配置文件说明

upstart服务配置文件`/etc/init/uwsgi.conf`:

```bash
# Emperor uWSGI script

description "uWSGI Emperor"
start on runlevel [2345]
stop on runlevel [06]

respawn

exec /opt/env/bin/uwsgi --ini /etc/uwsgi/emperor.ini
```

uwsgi emperor配置文件`/etc/uwsgi/emperor.ini`:

```ini
[uwsgi]
emperor = /etc/uwsgi/vassals
uid = scir-qa
gid = scir-qa
logto = /tmp/uwsgi.log
die-on-term = true
```
