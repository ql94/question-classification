#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify
import sys, os
import pycrfsuite
from sklearn.svm import SVC
from sklearn.externals import joblib
import numpy as np
from scipy import sparse
import logging
import codecs

logging.basicConfig(filename='log', level=logging.INFO, format='%(levelname)s - %(threadName)s - %(message)s')

app = Flask(__name__)

crf_model_path = "crf_model.crfsuite"
svm_model_path = "svm_model.pkl"
synonymPath = "synonym.txt"
semanticdictPath = "semanticdict.txt"
wordtablePath = "wordtable.txt"
stopwordPath = "stopword.txt"

def load_resources(synonymPath,semanticdictPath,wordtablePath,stopwordPath,crf_model_path,svm_model_path):
    semantic_dictionary = {}
    word_table = []
    stop_word = []
    semantic_table = []
    
    semanticdicFile = open(semanticdictPath,'r')
    for line in semanticdicFile.readlines():
        temp = line.replace('\r','').replace('\n','').split(' ')
        if temp[0] not in semantic_dictionary:
            semantic_dictionary[temp[0]] = temp[1][0:4]
    
    semanticdicFile.close()
    
    synonymFile = open(synonymPath,'r')
    semantic_table = synonymFile.readline().replace('\r','').replace('\n','').split(' ')
    synonymFile.close()
    
    wordtableFile = open(wordtablePath,'r')
    word_table = wordtableFile.readline().replace('\r','').replace('\n','').split(' ')
    wordtableFile.close()
    
    stopwordFile = open(stopwordPath,'r')
    stop_word=stopwordFile.readline().replace('\r','').replace('\n','').split(' ')
    stopwordFile.close()
    
    pos_table = ['z','a','ni','b','nl','c','ns','d','nt','e','nz','g','o','h','p','i','q','j','r','k','u','m','v','n','wp','nd','ws','nh','x']
    
    crf_model = pycrfsuite.Tagger()
    crf_model.open(crf_model_path)
    
    svm_model = joblib.load(svm_model_path)
    return word_table,semantic_dictionary,semantic_table,stop_word,pos_table,crf_model,svm_model

word_table,semantic_dictionary,semantic_table,stop_word,pos_table,crf_model,svm_model = load_resources(synonymPath,semanticdictPath,wordtablePath,      stopwordPath,crf_model_path,svm_model_path)

def get_crf_feature(words,postags,parents,relate):
    features = []
    for i in range(len(words)):
        feature = []
        if parents[i]!=-1:
            feature=["word="+words[i], "postag="+postags[i], "relatedword="+words[parents[i]],
                    "relatedpostag="+postags[parents[i]], "relation="+relate[i]]
        else:
            feature=["word="+words[i], "postag="+postags[i], "relatedword=<EOS>",
                    "relatedpostag=-", "relation="+relate[i]]
        features.append(feature)
    return features
def get_focus(features,crf_model):
    tags = crf_model.tag(features)
    fwords = []
    qwords = []
    for i in range(len(tags)):
        if tags[i] == "Q":
            qwords.append(features[i][0][5:])
        elif tags[i] == "F":
            fwords.append(features[i][0][5:])
    print qwords,fwords
    return qwords,fwords
def get_svm_feature(words,postags,qwords,fwords):
    size_word = len(word_table)
    size_sem = len(semantic_table)
    size_pos = len(pos_table)
    feature = []
    
    #非停用词/词性/语义
    for i in range(len(words)):
        if words[i] not in stop_word:
            if words[i] not in word_table:
                feature.append(0)
            else:
                feature.append(word_table.index(words[i])+1)
            if postags[i] == "%":
                print question
                postags[i] = "n"
            feature.append((size_word+1)+pos_table.index(postags[i]))
            if words[i] in semantic_dictionary:
                sem = semantic_dictionary[words[i]]
            else:
                sem = '-1'
            feature.append(semantic_table.index(sem)+(size_word+1)+size_pos)
    
    #疑问词/语义
    for qword in qwords:
        if qword in semantic_dictionary:
            sem = semantic_dictionary[qword]
        else:
            sem = '-1'
        if qword not in word_table:
            print qword
            feature.append((size_word+1)+size_pos+size_sem)
        else:
            feature.append(word_table.index(qword)+1+(size_word+1)+size_pos+size_sem)
        feature.append(semantic_table.index(sem)+2*(size_word+1)+size_pos+size_sem)
    
    #focus/语义
    for fword in fwords:
        if fword in semantic_dictionary:
            sem = semantic_dictionary[fword]
        else:
            sem = '-1'
        if fword not in word_table:
            feature.append(2*(size_word+1)+size_pos+2*size_sem)
        else:
            feature.append(word_table.index(fword)+1+2*(size_word+1)+size_pos+2*size_sem)
        
        feature.append(semantic_table.index(sem)+3*(size_word+1)+size_pos+2*size_sem)
    V = []
    ROW = []
    COL = []
    V.extend([1 for j in range(len(feature))])
    ROW.extend([0 for j in range(len(feature))])
    COL.extend([j for j in feature])
    realfeature = sparse.csr_matrix((V,(ROW,COL)),shape=(1,3*(size_word+1)+size_pos+3*size_sem))
    return realfeature

def question_classification(words,postags,parents,relate):
    crf_feature = get_crf_feature(words,postags,parents,relate)
    qwords,fwords = get_focus(crf_feature,crf_model)
    svm_feature = get_svm_feature(words,postags,qwords,fwords)
    result = svm_model.predict(svm_feature)
    label={1:'HUM',2:'LOC',3:'TIM',4:'NUM',5:'OTH'}
    return label[result[0]]

@app.route('/question-classification', methods=['POST'])
def handle():
    # start = time.time()
    data = request.get_json(force=True)
    words = [w.encode('utf-8') for w in data['seg']]
    postags = [p.encode('utf-8') for p in data['pos']]
    parents = [int(p) for p in data['parent']]
    relate = [r.encode('utf-8') for r in data['relate']]
    tag = question_classification(words,postags,parents,relate)
    print tag
    return jsonify({'tags': [tag]})
    
if __name__ == '__main__':
    #data = {"seg":["黑龙江","的","省会","在","哪"],"pos":["ns","u","n","p","r"],"parent":["2","0","3","-1","3"],"relate":["ATT","RAD","SBV","HED","POB"]}
    #words = data['seg']
    #postags = data['pos']
    #parents = [int(p) for p in data['parent']]
    #relate = data['relate']
    #result = question_classification(words,postags,parents,relate)
    #print result
    app.run(host='0.0.0.0', port=5000, debug=False)
